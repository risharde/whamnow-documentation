WhamNow Public Documentation
============================

DOWNLOADS
---------
You can download WhamNow Messenger here

* ANDROID: http://t.co.tt/custom/downloads/whamnow/whamnow.apk

API - SEND MESSAGE
------------------
You can send messages to any WhamNow user VIA the secure WEB API as long as you have the following information:

* Number
* Message


URL: https://t.co.tt/api/messaging/sendmessage

GET variables are as follows

* n (where n is the number in 10 digit format)
* m (where m is the message)

LIMITATIONS: The API endpoint will shorten the contents of each variable to a maximum of 512 characters.